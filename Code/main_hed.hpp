#ifndef MAIN_HED
#define MAIN_HED
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

#include "files_hed.hpp"
#include "fnaParser_hed.hpp"
#include "programSetting_hed.hpp"
#include "tables_hed.hpp"

#endif //MAIN_HED
