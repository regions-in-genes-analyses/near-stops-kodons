#ifndef FILES_HED
#define FILES_HED

#include <iostream>
#include <vector>
#include <fstream>
#include <memory>
#include <boost/filesystem.hpp>
#include "programSetting_hed.hpp"


class InFile
{
  public:
  boost::filesystem::path pathToFile;
  std::ifstream* inFile;
  InFile(boost::filesystem::path filePath);
  InFile(){inFile=new std::ifstream();}
  ~InFile();
  InFile(InFile&) =delete;
  InFile(InFile&& m) noexcept; 
  void open();
  void close();
  template <typename T>
    friend InFile& operator>> (InFile& str, T& data);
};

class OutFile
{
  public:
    boost::filesystem::path pathToFile;
    std::ofstream* outFile;
    OutFile(boost::filesystem::path filePath);
    OutFile(){outFile=new std::ofstream();}
    ~OutFile();
    OutFile(OutFile&)=delete;
    OutFile(OutFile&& m) noexcept;
    void recreate(boost::filesystem::path filePath);
    void open();
    void close();
    template<typename T>
      friend const OutFile& operator<< (const OutFile& str,const T& data);
};

class SpeciesFiles
{
  private:
    static int counter;
    std::ofstream* errorFile;
    std::string errorFilePath;
  public:
    std::string name;
    InFile in;
    OutFile ncf;
    OutFile dcf;
    OutFile adc;
    SpeciesFiles(boost::filesystem::path inFilePath, boost::filesystem::path outFilePath="");
    SpeciesFiles();
    SpeciesFiles(SpeciesFiles& m)=delete;
    SpeciesFiles(SpeciesFiles&& m) noexcept;
    ~SpeciesFiles();
    void printDebugInfo();
    void open();
    void close();
    std::ofstream* getErrorFile();
};

//---------------------

template<typename T>
InFile& operator>> (InFile& str, T& data)
{
  *(str.inFile)>>data;
  return str;
}


template <typename T>
const OutFile& operator<< (const OutFile& str,const T& data) 
{
  *(str.outFile)<<data;
  return str;
}
#endif //FILES_HED
