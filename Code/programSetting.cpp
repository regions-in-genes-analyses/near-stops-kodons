#include "programSetting_hed.hpp"


unsigned int ProgramSettings::omegaRandomType=(-1);
unsigned int ProgramSettings::minSequenceLengthInCodons=1;
unsigned int ProgramSettings::maxSequenceLengthInCodons=2000000000;
bool ProgramSettings::ifCommaInOutput=0;
bool ProgramSettings::ifGnuplotFile=0;
bool ProgramSettings::ifNewPatternForLokingCodons=0;
enum ProgramSettings::normalizationTypes ProgramSettings::normalizationType=remainder;
bool ProgramSettings::ifSharedGlobal_dcFile=0;
bool ProgramSettings::regionDebug=false;
std::ofstream* ProgramSettings::sharedGlobal_dcFile=nullptr;
std::ofstream* ProgramSettings::gnuplotFile=nullptr;

GenSplittingTemplate ProgramSettings::splittingTemplate(3,std::vector<int>{1,1,1});

std::vector<std::pair<std::vector<std::string>, std::string>> ProgramSettings::patternTablesForLookingCodons
{
  {DangerousCodonsTable(),"dangerous_codons"},
  {DangerousCodonsWithAlternative(), "with_alt"},
  {DangerousCodonsWithoutAlternative(), "no_alt"},
  {DoubleDangerousCodonsTable(), "double_danger"}
};

void ProgramSettings::addNewCodonGroupToPatternLookingTab(
    std::vector<std::string>&& group, std::string&& name)
{
  if(ifNewPatternForLokingCodons==0)
  {
    ifNewPatternForLokingCodons=1;
    patternTablesForLookingCodons.clear();
  }
  patternTablesForLookingCodons.emplace_back(std::make_pair(std::move(group), std::move(name)));
}

struct comma : std::numpunct<char> {
    char do_decimal_point()   const { return ','; }  // separate with comma
};
void ProgramSettings::createSharedGlobal_dcFile(std::string& s)
{
  sharedGlobal_dcFile=new std::ofstream();
  if(!boost::filesystem::exists(s))
  {
    boost::filesystem::create_directory(s);
  }
  if(!boost::filesystem::is_directory(s))
  {
    throw std::runtime_error("In --shared-global_dc   ->  "+s+" exist and isn't directory."); 
  }
  sharedGlobal_dcFile->open(s+"/shared_dc_global.txt");
  if(ProgramSettings::ifCommaInOutput==1)
  {
    sharedGlobal_dcFile->imbue(std::locale(sharedGlobal_dcFile->getloc(), new comma));
  }
}

void ProgramSettings::createGnuplotFile(std::string& s)
{
  gnuplotFile=new std::ofstream();
  if(!boost::filesystem::exists(s))
  {
    boost::filesystem::create_directories(s);
  }
  if(!boost::filesystem::is_directory(s))
  {
    throw std::runtime_error("In --gnuplot-file   ->  "+s+" exist and isn't directory."); 
  }
  gnuplotFile->open(s+"/gnuplot.txt");
  if(ProgramSettings::ifCommaInOutput==1)
  {
    gnuplotFile->imbue(std::locale(gnuplotFile->getloc(), new comma));
  }
}
std::ofstream* ProgramSettings::getSharedGlobal_dcFile()
{
  if(ifSharedGlobal_dcFile==0)
  {
    throw std::logic_error("Getting shared file, without create.");
  }
  return sharedGlobal_dcFile;
}

std::ofstream* ProgramSettings::getGnuplotFile()
{
  return gnuplotFile;
}
