#ifndef REGIONPROCESSING_HED
#define REGIONPROCESSING_HED

#include "programSetting_hed.hpp"
#include "GenSplitting_hed.hpp"
#include "tables_hed.hpp"
#include "GenSumarizeClass_hed.hpp"
#include "aminoacids_hed.hpp"

class RiskScoreOmega
{
  private:
    bool ifCorrectState;
  protected:
    ///sum (mayby partial) of omega sequence score
    double sequence;
    ///sum (mayby partial) of omega random score
    double random;

    ///add one codon, which is in GenRegion, to partial sequence sum score
    void AddToSequenceScore(const GenRegion&,const Codon& codon);
    ///add one codon, which is in GenRegion, to random sequence sum score
    void AddToRandomType0Score(const GenRegion& genRegion,const Codon& codon);
    void AddToRandomType1Score(const GenRegion& genRegion, const Codon& codon, const GenInfo& genI);
    void AddToRandomType2Score(const GenRegion& genRegion, const Codon& codon, const GenInfo& genI);
  public:
    ///construct RiskScoreOmega and populate omega sequence score and
    ///omega random score with correct values(sums)
    RiskScoreOmega(const GenRegion&, const GenInfo&);
    RiskScoreOmega(const RiskScoreOmega& O);
    RiskScoreOmega(const bool);

    
    ///return sequence score
    double getSequenceScore() const;
    ///return random score
    double getRandomScore() const;

    ///return F
    double getF() const;
};

class RegionResult
{
  private:
    ///variable which tell us if Frequencies have correct initialized values 
    bool ifFrequenciesAreCorrect;
  protected:
    double realFrequency;
    double normalizedFrequency;
    double predictedFrequency;
  public:
    bool ifErrorOccur;
    int regionNumber;
    std::string regionLabel;
    long long countOfLookedCodons;
    long long regionLengthInCodons;

    RiskScoreOmega omega;

    RegionResult(int n, long long c, long long l, std::string, const RiskScoreOmega&);
    RegionResult();
    RegionResult(int n, bool e);

    double getRealFrequency() const;

    double getNormalizedFrequency() const;

    double getPredictedFrequency() const;

    ///count and set normalize frequency on the base of GenInfo
    void setFrequencies(const GenInfo&, const std::vector<std::string>&);

    void printDebugInfo();
    RegionResult& operator+ (const RegionResult&);
};

RegionResult lookForCodonsInRegion(const GenRegion&, const std::vector<std::string>&, const GenInfo&);

#endif //REGIONPROCESSING_HED
