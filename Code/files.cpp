#include "files_hed.hpp"

int SpeciesFiles::counter=0;

struct comma : std::numpunct<char> {
    char do_decimal_point()   const { return ','; }  // separate with comma
};

InFile::InFile(boost::filesystem::path filePath)
{
  inFile=new std::ifstream();
  pathToFile=filePath;
}
void InFile::open()
{
  if(inFile->is_open())
  {
    return;
  }
  inFile->open(pathToFile.generic_string());
  if(inFile->fail())
  {
    std::perror(NULL);
    throw std::ios_base::failure("Error, while opening a in file: "+pathToFile.string());
  }
}
void InFile::close()
{
  if(inFile->is_open())
  {
    inFile->close();
  }
}

InFile::~InFile()
{
  if(inFile!=nullptr and inFile->is_open())
  {
    inFile->close();
  }
  delete inFile;
}
InFile::InFile(InFile&& m) noexcept
{
  this->inFile=m.inFile;
  this->pathToFile=m.pathToFile;
  m.inFile=nullptr;
}

OutFile::OutFile(boost::filesystem::path filePath)
{
  outFile=new std::ofstream();
  pathToFile=filePath.generic_string();
}

void OutFile::open()
{
  if(outFile->is_open())
  {
    return;
  }
  outFile->open(pathToFile.generic_string());
  outFile->precision(9);
  if(ProgramSettings::ifCommaInOutput==1)
  {
    outFile->imbue(std::locale(outFile->getloc(), new comma));
  }
  if(outFile->fail())
  {
    throw std::ios_base::failure("Error, while opening out file: "+pathToFile.generic_string());
  }

}

void OutFile::close()
{
  if(outFile->is_open())
  {
    outFile->close();
  }
}

OutFile::~OutFile()
{
  if(outFile!=nullptr and outFile->is_open())
  {
    outFile->close();
  }
  delete outFile;
}
OutFile::OutFile(OutFile&& m) noexcept
{
  this->outFile=m.outFile;
  this->pathToFile=m.pathToFile;
  m.outFile=nullptr;
}
void OutFile::recreate(boost::filesystem::path filePath)
{
  if(outFile->is_open())
  {
    outFile->close();
  }
  outFile->open(filePath.generic_string());
  outFile->precision(9);
  if(ProgramSettings::ifCommaInOutput==1)
  {
    outFile->imbue(std::locale(outFile->getloc(), new comma));
  }
  if(outFile->fail())
  {
    throw std::ios_base::failure("Error, while recreating out file: "+filePath.string());
  }
}

SpeciesFiles::~SpeciesFiles()
{
  if(errorFile!=nullptr)
  {
    delete errorFile;
  }
}
SpeciesFiles::SpeciesFiles(SpeciesFiles&& m) noexcept :name(std::move(m.name)),in(std::move(m.in)),
  ncf(std::move(m.ncf)),dcf(std::move(m.dcf)),adc(std::move(m.adc)) 
{ 
  this->errorFile=m.errorFile;
  this->errorFilePath=std::move(m.errorFilePath);
  m.errorFile=nullptr;
}
SpeciesFiles::SpeciesFiles(boost::filesystem::path inFilePath, boost::filesystem::path outFilePath):in(inFilePath)
{
  counter++;
  errorFile=nullptr;
  ///Construct SpeciesFiles wich contains in and out files.
  ///They have the same species.
  name=inFilePath.filename().generic_string();
  std::string defaultPath;
  if(outFilePath!="")
  {
    defaultPath=outFilePath.generic_string();
    if(!boost::filesystem::exists(outFilePath.parent_path()))
    {
      boost::filesystem::create_directories(outFilePath.parent_path());
    }
  }
  else
  {
    defaultPath=inFilePath.stem().generic_string();
  }
  ncf.pathToFile=boost::filesystem::path(defaultPath+"_c_compose.txt");
  dcf.pathToFile=boost::filesystem::path(defaultPath+"_dc_genes.txt");
  if(ProgramSettings::ifSharedGlobal_dcFile==0)
  {
    adc.pathToFile=boost::filesystem::path(defaultPath+"_dc_global.txt");
  }
  else
  {
    adc.outFile=ProgramSettings::getSharedGlobal_dcFile();
  }
  errorFilePath=defaultPath+".error";
}

void SpeciesFiles::open()
{
  if(!adc.outFile->is_open())
  {
    adc.open();
  }
  ncf.open();
  dcf.open();
  in.open();
}

void SpeciesFiles::close()
{
  in.close();
  ncf.close();
  dcf.close();
  counter--;
  if(ProgramSettings::ifSharedGlobal_dcFile==1 && counter!=0)
  {
    adc.outFile=nullptr;
  }
  else
  {
    adc.close();
  }
  if(errorFile!=nullptr && errorFile->is_open())
  {
    errorFile->close();
  }
}

SpeciesFiles::SpeciesFiles()
{
  counter++;
  errorFile=nullptr;
  errorFilePath="NoName.error";
}

void SpeciesFiles::printDebugInfo()
{
  std::cerr<<"InStream is_open?: "<<in.inFile->is_open()<<"\n";
  std::cerr<<".ncf is_open?: "<<ncf.outFile->is_open()<<"\n";
  std::cerr<<".dcf is_open?: "<<dcf.outFile->is_open()<<"\n";
}
std::ofstream* SpeciesFiles::getErrorFile()
{
  if(errorFile==nullptr)
  {
    errorFile=new std::ofstream();
    errorFile->open(errorFilePath);
  }
  return errorFile;
}

