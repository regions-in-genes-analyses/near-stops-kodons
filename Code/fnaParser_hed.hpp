#ifndef FNAPARSER_HED
#define FNAPARSER_HED

#include "programSetting_hed.hpp"
#include "GenSumarizeClass_hed.hpp"
#include "files_hed.hpp"
#include "GenSplitting_hed.hpp"
#include "GenClass_hed.hpp"
#include "tables_hed.hpp"
#include "regionProcessing_hed.hpp"
#include "excpetions_hed.hpp"

class GenomeResult:public RegionResult
{
  public:
    double sumOfNormalizedFrequency;
    double sumOfPredictedFrequency;
    double sumOfFrequency;
    std::string regionLabel;
    long long countOfGens;
    GenomeResult& operator+ (const RegionResult&);
    GenomeResult(int regNum);
    GenomeResult();
    double getAvarangeOfNormalizedFrequencies() const;
    double getAvarangeOfPredictedFrequencies() const;
    double getAvarangeOfFrequencies() const;
};
typedef GenSumarize GenomeSumarize;
typedef GenInfo GenomeInfo;

void parseOneFnaFile(SpeciesFiles& speciesFiles);
void printHeaderOfADCFile(const SpeciesFiles& speciesFiles);


#endif //FNAPARSER_HED
