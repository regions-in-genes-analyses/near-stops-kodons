#include "aminoacids_hed.hpp"

std::vector<char> nucleotideCharList={'A','T','C','G'};

std::vector<char> Aminoacid::aminoacidsList=
{'A','C','D','E','F','G','H','I','K','L','M','N','P','R','S','T','W','Q','V','X','Y'};

std::unordered_map<char,std::vector<Codon>> Aminoacid::codonsCodingAminoacid=
{
  {'A',std::vector<Codon>{Codon("GCA"),Codon("GCC"), Codon("GCG"),Codon("GCT")}},
  {'C',std::vector<Codon>{Codon("TGC"),Codon("TGT")}},
  {'D',std::vector<Codon>{Codon("GAC"),Codon("GAT")}},
  {'E',std::vector<Codon>{Codon("GAA"),Codon("GAG")}},
  {'F',std::vector<Codon>{Codon("TTC"),Codon("TTT")}},
  {'G',std::vector<Codon>{Codon("GGA"),Codon("GGC"),Codon("GGG"),Codon("GGT")}},
  {'H',std::vector<Codon>{Codon("CAC"),Codon("CAT")}},
  {'I',std::vector<Codon>{Codon("ATA"),Codon("ATC"),Codon("ATT")}},
  {'K',std::vector<Codon>{Codon("AAA"),Codon("AAG")}},
  {'L',std::vector<Codon>{Codon("TTA"),Codon("TTG"),Codon("CTA"),Codon("CTC"),Codon("CTG"),Codon("CTT")}},
  {'M',std::vector<Codon>{Codon("ATG")}},
  {'N',std::vector<Codon>{Codon("AAC"),Codon("AAT")}},
  {'P',std::vector<Codon>{Codon("CCA"),Codon("CCC"),Codon("CCG"),Codon("CCT")}},
  {'R',std::vector<Codon>{Codon("AGA"),Codon("AGG"),Codon("CGA"),Codon("CGC"),Codon("CGG"),Codon("CGT")}},
  {'S',std::vector<Codon>{Codon("AGC"),Codon("AGT"),Codon("TCA"),Codon("TCC"),Codon("TCG"),Codon("TCT")}},
  {'T',std::vector<Codon>{Codon("ACA"),Codon("ACC"),Codon("ACG"),Codon("ACT")}},
  {'W',std::vector<Codon>{Codon("TGG")}},
  {'Q',std::vector<Codon>{Codon("CAA"),Codon("CAG")}},
  {'V',std::vector<Codon>{Codon("GTA"),Codon("GTC"),Codon("GTG"),Codon("GTT")}},
  {'X',std::vector<Codon>{Codon("TAA"),Codon("TAG"),Codon("TGA")}},
  {'Y',std::vector<Codon>{Codon("TAC"),Codon("TAT")}}
};

Nucleotide::Nucleotide(char s)
{
  if(nucleotideCharList.end()==std::find(nucleotideCharList.begin(),nucleotideCharList.end(),s))
  {
    throw NucleotideException ("Not correct nucleotide in Nucleotide::Nucleotide(char s).");
  }
  c=s;
  n=charToInt();
}

int Nucleotide::charToInt() const
{
  switch (c)
  {
    case 'A':
      return 0;
    case 'T':
      return 1;
    case 'C':
      return 2;
    case 'G':
      return 3;
    default:
      throw GenException ("In function getCodonCount char is not correct nucleotide.");
  }
}

int Nucleotide::getNucleotideInt() const noexcept
{
  return n;
}

char Nucleotide::getNucleotideChar() const noexcept
{
  return c;
}

Codon::Codon(const std::string& seq):Codon(seq[0],seq[1],seq[2])
{}

Codon::Codon(const char c1, const char c2, const char c3):n{c1,c2,c3}
{}

Codon::Codon(const Nucleotide& N1, const Nucleotide& N2, const Nucleotide& N3):
  n{N1,N2,N3}
{}

int Codon::getNucleotideInt(int p) const
{
  if(p>=3)
  {
    throw std::logic_error ("Codon has only 3 nucleotide, so p should be in {0,1,2}");
  }
  return n[p].getNucleotideInt();
}
char Codon::getNucleotideChar(int p) const
{
  if(p>=3)
  {
    throw std::logic_error ("Codon has only 3 nucleotide, so p should be in {0,1,2}");
  }
  return n[p].getNucleotideChar();
}

std::string Codon::getString() const
{
  char repr[3];
  repr[0]=this->getNucleotideChar(0);
  repr[1]=this->getNucleotideChar(1);
  repr[2]=this->getNucleotideChar(2);
  return std::string(repr);
}

Aminoacid::Aminoacid(const Codon& s):seq(s)
{
  symbol=DNACodonTable[seq.getNucleotideInt(0)][seq.getNucleotideInt(1)][seq.getNucleotideInt(2)];
}
Aminoacid::Aminoacid(const char c1, const char c2, const char c3):Aminoacid(Codon(c1,c2,c3))
{ }

char Aminoacid::getSymbol() const noexcept
{
  return symbol;
}

int Aminoacid::codonsCount() const noexcept
{
  return codonsCodingAminoacid[symbol].size();
}

std::vector<Codon> Aminoacid::codingCodons() const noexcept
{
  return codonsCodingAminoacid[symbol];
}

const std::vector<char>& Aminoacid::getListOfAminoacids()
{
  return aminoacidsList;
}

const std::vector<Codon>& Aminoacid::codingCodons(char aa)
{
  return codonsCodingAminoacid[aa];
}
