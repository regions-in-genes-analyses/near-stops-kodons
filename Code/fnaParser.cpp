#include "fnaParser_hed.hpp"

GenomeResult::GenomeResult(int regNum)
{
  regionNumber=regNum;
  countOfGens=0;
  sumOfNormalizedFrequency=0;
  sumOfPredictedFrequency=0;
  sumOfFrequency=0;
}
GenomeResult& GenomeResult::operator+ (const RegionResult& result)
{
  if(result.ifErrorOccur)
  {
    return *this;
  }
  if(this->regionNumber==-1)
  {
    this->regionLabel=result.regionLabel;
    this->regionNumber=result.regionNumber;
  }
  this->countOfLookedCodons+=result.countOfLookedCodons;
  this->regionLengthInCodons+=result.regionLengthInCodons;
  this->countOfGens++;
  this->sumOfFrequency+=result.getRealFrequency();
  this->sumOfNormalizedFrequency+=result.getNormalizedFrequency();
  this->sumOfPredictedFrequency+=result.getPredictedFrequency();
  return *this;
}
double GenomeResult::getAvarangeOfNormalizedFrequencies() const
{
  return sumOfNormalizedFrequency/(double)countOfGens;
}
double GenomeResult::getAvarangeOfPredictedFrequencies() const
{
  return sumOfPredictedFrequency/(double)countOfGens;
}
double GenomeResult::getAvarangeOfFrequencies() const
{
  return sumOfFrequency/(double)countOfGens;
}
GenomeResult::GenomeResult()
{
  regionNumber=-1;
  countOfGens=0;
  sumOfNormalizedFrequency=0;
}

void removeStopsCodonsFromEnd(std::string& gen)
///Take gen sequence as string and remove all stops codons from end
{
  while(gen.size()>0)
  {
    bool founded=false;
    for(const Codon& stop: Aminoacid::codingCodons('X'))
    {
      if(gen.substr(gen.size()-3)==stop.getString())
      {
        gen.erase(gen.size()-3);
        founded=true;
        break;
      }
    }
    if(founded==false)
    {
      break;
    }
  }
}

void loadOneGenFromFile(std::string& oneGenString, Gen& gen)
  ///take description of sequence and sequence as string and initialize instance of Gen class
{
  std::string sequenceWithNL;
  gen.errorMessage="-";
  int newLinePosition=oneGenString.find('\n');
  sequenceWithNL=oneGenString.substr(newLinePosition+1);
  if(oneGenString[newLinePosition-1]==13)///< when we have windows file with CRNL end of line
  {
    newLinePosition--;
  }
  gen.header=oneGenString.substr(0,newLinePosition);

  int seqSize=sequenceWithNL.size();
  for(int i=0;i<seqSize;i++)
  {
    if(!std::isspace(sequenceWithNL[i]))///< if char is not white space
      gen.sequence+=sequenceWithNL[i];
  }
  if(gen.sequence.size()<=3)
  {
    gen.lastTreeNucleotides=gen.sequence;
    throw TooShortGenException("Sequence is too short. Gen omittet.");
  }
  gen.lastTreeNucleotides=gen.sequence.substr(gen.sequence.size()-3);
  if(gen.sequence.size()/3<ProgramSettings::minSequenceLengthInCodons ||
      gen.sequence.size()/3>ProgramSettings::maxSequenceLengthInCodons)
  {
    throw GenToOmit("");
  }
  std::string message="Sequence isn't divided by 3. Following nucleotides was drop: ";
  if(gen.sequence.size()%3==0)
  {
    //remove stop codon
    removeStopsCodonsFromEnd(gen.sequence);
    if(gen.sequence.size()<3)
    {
      throw TooShortGenException("Sequence is too short. Gen omittet.");
    }
  }
  else if(gen.sequence.size()%3==1)
  {
    message+=gen.sequence[gen.sequence.size()-1];
    gen.sequence.erase(gen.sequence.size()-1);
    gen.errorMessage=message;
    removeStopsCodonsFromEnd(gen.sequence);
    if(gen.sequence.size()<3)
    {
      throw TooShortGenException("Sequence is too short. Gen omittet.");
    }
    throw NonTreeDividedGenException(message);
  }
  else
  {
    message+=gen.sequence[gen.sequence.size()-2];
    message+=gen.sequence[gen.sequence.size()-1];
    gen.sequence.erase(gen.sequence.size()-2);
    gen.errorMessage=message;
    removeStopsCodonsFromEnd(gen.sequence);
    if(gen.sequence.size()<3)
    {
      throw TooShortGenException("Sequence is too short. Gen omittet.");
    }
    throw NonTreeDividedGenException(message);
  }
}

void printHeaderOfNCFFile(SpeciesFiles& speciesFiles, int regionCount)
{
  speciesFiles.ncf<<"name\tA1\tT1\tC1\tG1\t"<<
    "A2\tT2\tC2\tG2\tA3\tT3\tC3\tG3\tA\tT\tC\tG\t"<<
    "AT\tGC\t"<<"ORF_length\tstart_codon\tstop_codon\tatypical_nt_number\terror_message\t";

  //aminoacids

  //0-A, 1-T, 2-C, 3-G
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        speciesFiles.ncf<<DNACodonTable[i][j][k]<<"_";
        char codon[4];
        int tab[3]={i,j,k};
        for(int n=0;n<3;n++)
        {
          switch (tab[n])
          {
            case 0:
              codon[n]='A';
              break;
            case 1:
              codon[n]='T';
              break;
            case 2:
              codon[n]='C';
              break;
            case 3:
              codon[n]='G';
              break;
          }
        }
        codon[3]='\0';
        speciesFiles.ncf<<codon<<"\t";
      }
    }
  }
  //Predicted codon freuency
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        speciesFiles.ncf<<"PF_"<<DNACodonTable[i][j][k]<<"_";
        char codon[4];
        int tab[3]={i,j,k};
        for(int n=0;n<3;n++)
        {
          switch (tab[n])
          {
            case 0:
              codon[n]='A';
              break;
            case 1:
              codon[n]='T';
              break;
            case 2:
              codon[n]='C';
              break;
            case 3:
              codon[n]='G';
              break;
          }
        }
        codon[3]='\0';
        speciesFiles.ncf<<codon<<"\t";
      }
    }
  }

  //headers of region codon frequencies
  
  std::vector<std::string> labelsOfCodonFreqsInRegion=GenRegion::labelsOfvectorOfCodonsFrequencies();

  for(int i=1;i<=regionCount;i++)
  {
    for(const std::string& label: labelsOfCodonFreqsInRegion)
    {
      speciesFiles.ncf<<label<<std::to_string(i)<<"\t";
    }
  }

  speciesFiles.ncf<<"\n";

}

void printOneLineOfNCFFile(Gen& gen, GenSumarize& genS,  SpeciesFiles& speciesFiles, 
    GenInfo& genI, const std::vector<GenRegion>& regs)
{
  //TODO:when adding concurency here we have to pass on races
  speciesFiles.ncf<<gen.header<<"\t";
  long long correctNucleotideSum=genS.getSumOfAllCorrectNucleotide();
  double nucleotideSum=correctNucleotideSum+genS.getNonCorrectNucleotide();

  genI.createNucleotideFreqencyTable(genS);
  genI.createCodonFrequencyTable(genS);
  genI.createCodonPredictedFrequencyTable();
  //A1,T1,C1,G1
  //A2,T2,C2,G2
  //A3,T3,C3,G3
  for(int p=0;p<3;p++)
  {
    for(int n=0;n<4;n++)
    {
      speciesFiles.ncf<<genI.getFrequecyOfNucleotideOnPosition(n,p)<<"\t";
    }
  }
  //A,T,G,C
  speciesFiles.ncf<<genS.getNucleotideSum('A')/nucleotideSum<<"\t";
  speciesFiles.ncf<<genS.getNucleotideSum('T')/nucleotideSum<<"\t";
  speciesFiles.ncf<<genS.getNucleotideSum('C')/nucleotideSum<<"\t";
  speciesFiles.ncf<<genS.getNucleotideSum('G')/nucleotideSum<<"\t";
  //AT,GC
  speciesFiles.ncf<<(genS.getNucleotideSum('A')+
      genS.getNucleotideSum('T'))/nucleotideSum<<"\t";
  speciesFiles.ncf<<(genS.getNucleotideSum('G')+
      genS.getNucleotideSum('C'))/nucleotideSum<<"\t";
  //ORF_length
  speciesFiles.ncf<<gen.sequence.size()<<"\t";
  //start_codon
  speciesFiles.ncf<<gen.sequence.substr(0,3)<<"\t";
  //stop_codon
  speciesFiles.ncf<<gen.lastTreeNucleotides<<"\t";
  //atypical_nt
  speciesFiles.ncf<<genS.getNonCorrectNucleotide()<<"\t";
  //error_message
  speciesFiles.ncf<<gen.errorMessage<<"\t";

  //amino acids

  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        speciesFiles.ncf<<genI.getCodonFreqeuncy(i,j,k)<<"\t";
      }
    }
  }

  //predicted aminoacids frequency

  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        speciesFiles.ncf<<genI.getPredictedCodonFrequency(i,j,k)<<"\t";
      }
    }
  }

  for(const GenRegion& region:regs)
  { 
    std::vector<double> codonsFreqsInRegion=region.vectorOfCodonsFrequencies();
    for(double freq: codonsFreqsInRegion)
    {
      speciesFiles.ncf<<freq<<"\t";
    }
  }

  speciesFiles.ncf<<"\n";
}

GenSumarize countingNucleotidesAndAminoacidsInOneGen(Gen& gen)
{
  GenSumarize genS;
  int genSequenceSize=gen.sequence.size();
  if(genSequenceSize%3!=0)
  {
    throw GenException("Gen haven't 3*n (where n is integer) codons.");
  }
  for(int k=0;k<genSequenceSize;k++)
  {
    genS.addNucleotide(gen.sequence[k],k%3);
    if(k%3==2)
    {
      genS.addCodon(gen.sequence[k-2],gen.sequence[k-1],gen.sequence[k]);
    }
  }
  return genS;
}

void printGenErrorLine(SpeciesFiles& speciesFiles, const Gen& gen, const std::string& error)
{
  (*speciesFiles.getErrorFile())<<"Error of type: "<<error<<" in gen: "<<gen.header<<"\n";
  std::cerr<<"Error of type: "<<error<<" in gen: "<<gen.header<<"\n";
  return;
}
void printFnaFileErrorLine(SpeciesFiles& speciesFiles, const std::string& error)
{
  (*speciesFiles.getErrorFile())<<"Error of type: "<<error<<" in .fna file.\n";
}

void printHeaderOfDCFFile(SpeciesFiles& speciesFiles)
{
  speciesFiles.dcf<<"seq_name\ttype\t";
  for(unsigned int i=0;i<ProgramSettings::splittingTemplate.regionLength.size();i++)
  {
    speciesFiles.dcf<<"reg"<<i<<"_Cnum\t";
    speciesFiles.dcf<<"reg"<<i<<"_size\t";
    if(ProgramSettings::normalizationType<2)
    {
      speciesFiles.dcf<<"reg"<<i<<"_freq\t";
      speciesFiles.dcf<<"reg"<<i<<"_freq_exp\t";
      speciesFiles.dcf<<"reg"<<i<<"_freq_normal\t";
    }
    else if (ProgramSettings::normalizationType==2)
    {
      speciesFiles.dcf<<"reg"<<i<<"_\u03a9_sequence\t";
      speciesFiles.dcf<<"reg"<<i<<"_\u03a9_random\t";
      speciesFiles.dcf<<"reg"<<i<<"_F\t";
    }
  }
  speciesFiles.dcf<<"\n";
}

void printLineOfDCFFile(const SpeciesFiles& speciesFiles, const std::string genName, 
    const std::string type, std::vector<RegionResult>& results)
{
  speciesFiles.dcf<<genName<<"\t"<<type<<"\t";
  for(unsigned int i=0;i<results.size();i++)
  {
    speciesFiles.dcf<<results[i].countOfLookedCodons<<"\t";
    speciesFiles.dcf<<results[i].regionLengthInCodons<<"\t";
    speciesFiles.dcf<<results[i].getRealFrequency()<<"\t";
    speciesFiles.dcf<<results[i].getPredictedFrequency()<<"\t";
    speciesFiles.dcf<<results[i].getNormalizedFrequency()<<"\t";
  }
  speciesFiles.dcf<<"\n";
}


void genParserDiespather(Gen gen, SpeciesFiles& speciesFiles, 
    std::vector<std::vector<RegionResult>>& genResults, GenSumarize& genS)
{
  try
  {
    genS=countingNucleotidesAndAminoacidsInOneGen(gen);
    GenInfo genI;
    //printOneLineOfNCFFile initialize genI

    std::vector<GenRegion> regs;
    splitGenAccordingToResolution(gen, ProgramSettings::splittingTemplate, regs);
    combineSplitedGenAccordingToTemplate(ProgramSettings::splittingTemplate, regs);
    if (ProgramSettings::regionDebug)
    {
      std::cerr<<gen.header<<"\n";
      for(unsigned int i=0;i<regs.size();i++)
      {
        std::cerr<<regs[i]<<"\n";
      }
      std::cerr<<"\n";
    }

    printOneLineOfNCFFile(gen, genS, speciesFiles,genI, regs);

    const std::vector<std::pair<std::vector<std::string>, std::string>>& pattern=
      ProgramSettings::getPatternTabForLookingCodons();
    genResults.clear();
    genResults.resize(pattern.size());
    for(unsigned int i=0;i<pattern.size();i++)
    {
      for(unsigned int j=0;j<regs.size();j++)
      {
        RegionResult r=lookForCodonsInRegion(regs[j],pattern[i].first, genI);
        r.setFrequencies(genI, pattern[i].first);
        genResults[i].push_back(r);
      }
      printLineOfDCFFile(speciesFiles,gen.header,pattern[i].second,genResults[i]);
    }
  }
  catch(GenException& error)
  {
    printGenErrorLine(speciesFiles, gen, error.what());
    genResults.clear();
    genResults.resize(ProgramSettings::getPatternTabForLookingCodons().size());
    for(unsigned int i=0;i<genResults.size();i++)
    {
      genResults[i].resize(ProgramSettings::splittingTemplate.regionLength.size());
      for(unsigned int j=0;j<genResults[i].size();j++)
      {
        genResults[i][j]=RegionResult(j,true);
      }
    }
    genS=GenSumarize();
  }
}

void printHeaderOfADCFile(const SpeciesFiles& speciesFiles)
{
  speciesFiles.adc<<"seq_name\tregion\ttype\treg_Cnum\treg_size\treg_freq\treg_freq_exp\treg_freq_normal\n";
  return;
}

void printADCFile(const SpeciesFiles& speciesFiles, 
    const std::vector<std::vector<GenomeResult>>& genomeResult)
  ///genomeResult[i][j] - is the genomeResult for i-th pattern and for j-th region
{
  const std::vector<std::pair<std::vector<std::string>,std::string>> pattern=
    ProgramSettings::getPatternTabForLookingCodons();
  for(unsigned int i=0;i<pattern.size();i++)
  {
    for(unsigned int j=0;j<genomeResult[i].size();j++)
    {
      speciesFiles.adc<<speciesFiles.name<<"\t";
      speciesFiles.adc<<genomeResult[i][j].regionLabel<<"\t";
      speciesFiles.adc<<pattern[i].second<<"\t";
      speciesFiles.adc<<genomeResult[i][j].countOfLookedCodons<<"\t";
      speciesFiles.adc<<genomeResult[i][j].regionLengthInCodons<<"\t";
      speciesFiles.adc<<genomeResult[i][j].getAvarangeOfFrequencies()<<"\t";
      speciesFiles.adc<<genomeResult[i][j].getAvarangeOfPredictedFrequencies()<<"\t";
      speciesFiles.adc<<genomeResult[i][j].getAvarangeOfNormalizedFrequencies()<<"\n";
    }
  }
}

void printADCGnuplotFile(const SpeciesFiles& specieFiles,std::ofstream* gnuplotFile, 
    const std::vector<std::vector<GenomeResult>>& genomeResult)
  ///genomeResult[i][j] - is the genomeResult for i-th pattern and for j-th region
{
  const std::vector<std::pair<std::vector<std::string>,std::string>> pattern=
    ProgramSettings::getPatternTabForLookingCodons();
  for(unsigned int i=0;i<pattern.size();i++)
  {
    (*gnuplotFile)<<specieFiles.name<<"\t";
    (*gnuplotFile)<<pattern[i].second<<"\n";
    for(unsigned int j=0;j<genomeResult[i].size();j++)
    {
      (*gnuplotFile)<<genomeResult[i][j].regionLabel<<"\t";
      (*gnuplotFile)<<genomeResult[i][j].getAvarangeOfNormalizedFrequencies()<<"\n";
    }
    (*gnuplotFile)<<"\n\n";
  }
}
void getOneGenString(const SpeciesFiles& speciesFiles, std::string& oneGenString)
  ///load one sequence with description from fasta file into string
{
  std::getline(*speciesFiles.in.inFile,oneGenString);
  std::string help;
  oneGenString+="\n";
  std::getline(*speciesFiles.in.inFile,help,'>');
  oneGenString+=help;
}

void parseOneFnaFile(SpeciesFiles& speciesFiles)
{
  try
  {
    using namespace std;
    string oneGenString;

    std::getline(*speciesFiles.in.inFile,oneGenString,'>');//remove all to first '>'

    printHeaderOfNCFFile(speciesFiles, ProgramSettings::splittingTemplate.regionLength.size());
    printHeaderOfDCFFile(speciesFiles);
    if(ProgramSettings::ifSharedGlobal_dcFile==0)
    {
      printHeaderOfADCFile(speciesFiles);
    }

    std::vector<std::vector<RegionResult>> regionsResults;
    GenomeSumarize genomeS;

    vector<vector<GenomeResult>> genomeResult;
    genomeResult.resize(ProgramSettings::getPatternTabForLookingCodons().size());
    for(unsigned int i=0;i<genomeResult.size();i++)
    {
      genomeResult[i].resize(ProgramSettings::splittingTemplate.regionLength.size());
      for(unsigned int j=0;j<genomeResult[i].size();j++)
      {
        genomeResult[i][j]=GenomeResult(-1);
      }
    }

    while(!speciesFiles.in.inFile->eof())
    {
      GenSumarize genS;
      getOneGenString(speciesFiles, oneGenString);
      Gen gen;
      try
      {
        loadOneGenFromFile(oneGenString, gen);
      }
      catch(GenToOmit& error)
      {
        continue;
      }
      catch(TooShortGenException& error)
      {
        printGenErrorLine(speciesFiles,gen,error.what());
        continue;
      }
      catch(GenException& error)
      {
        printGenErrorLine(speciesFiles,gen,error.what());
      }
      genParserDiespather(gen, speciesFiles, regionsResults, genS);
      genomeS.combineGens(genS);
      for(unsigned int i=0;i<regionsResults.size();i++)
      {
        if(regionsResults[i].size()!=genomeResult[i].size())
        {
          throw FnaFileException ("GenomeResult and regionsResults hasn't the same number of regions.");
        }
        for(unsigned int j=0;j<regionsResults[i].size();j++)
        {
          genomeResult[i][j]=genomeResult[i][j]+regionsResults[i][j];
        }
      }
    }

    printADCFile(speciesFiles,genomeResult);
    if(ProgramSettings::ifGnuplotFile)
    {
      printADCGnuplotFile(speciesFiles, ProgramSettings::getGnuplotFile(), genomeResult);
    }
  }
  catch(FnaFileException& error)
  {
    printFnaFileErrorLine(speciesFiles,error.what());
  }
}

