#include "GenSumarizeClass_hed.hpp"

GenSumarize::GenSumarize()
{ }

GenSumarize& GenSumarize::combineGens(const GenSumarize& genS)
{
  nonCorrectNucleotide+=genS.nonCorrectNucleotide;
  nonCorrectCodon+=genS.nonCorrectCodon;
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      ATCG[i][j]+=genS.ATCG[i][j];
    }
  }
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        codonsTable[i][j][k]+=genS.codonsTable[i][j][k];
      }
    }
  }
  return *this;
}

GenCodons::GenCodons()
{
  nonCorrectCodon=0;
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        codonsTable[i][j][k]=0;
      }
    }
  }
}

GenNucleotides::GenNucleotides()
{
  nonCorrectNucleotide=0;
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      ATCG[i][j]=0;
    }
  }
}

void GenNucleotides::addNucleotide(const char c,const unsigned short k)
{
  switch (c)
  {
    case 'A':
      ATCG[k][0]++;
      break;
    case 'T':
      ATCG[k][1]++;
      break;
    case 'C':
      ATCG[k][2]++;
      break;
    case 'G':
      ATCG[k][3]++;
      break;
    default:
      nonCorrectNucleotide++;
  }
  return;
}

void GenCodons::addCodon(const char c1 ,const char c2,const char c3)
{
  char codonT[3]={c1,c2,c3};
  short tab[3]={-1,-1,-1};
  for(int i=0;i<3;i++)
  {
    switch (codonT[i])
    {
      case 'A':
        tab[i]=0;
        break;
      case 'T':
        tab[i]=1;
        break;
      case 'C':
        tab[i]=2;
        break;
      case 'G':
        tab[i]=3;
        break;
      default:
        nonCorrectCodon++;
        return;
    }
  }
  codonsTable[tab[0]][tab[1]][tab[2]]++;
  return;
}

long long GenNucleotides::getSumOfAllCorrectNucleotide() const
{
  long long sum=0;
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      sum+=ATCG[i][j];
    }
  }
  return sum;
}

long long GenNucleotides::getNonCorrectNucleotide() const
{
  return nonCorrectNucleotide;
}

long long GenCodons::getSumOfCorrectCodons() const
{
  unsigned long long sum=0;
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        sum+=codonsTable[i][j][k];
      }
    }
  } 
  return sum;
}
long long GenCodons::getNonCorrectCodons() const
{
  return nonCorrectCodon;
}

long long GenNucleotides::getNucleotideSum(const int k) const
{
  long long sum=0;
  for(int i=0;i<4;i++)
  {
    sum+=ATCG[k][i];
  }
  return sum;
}

long long GenNucleotides::getNucleotides(const char c, const int k) const
{
  switch (c)
  {
    case 'A':
      return ATCG[k][0];
    case 'T':
      return ATCG[k][1];
    case 'C':
      return ATCG[k][2];
    case 'G':
      return ATCG[k][3];
  }
  throw GenException("In function GenSumarize::getNucleotides char is not a nucleotide.");
}

long long GenNucleotides::getNucleotides(const int n, const int p) const
{
  return ATCG[p][n];
}

long long GenNucleotides::getNucleotideSum(const char c) const
{
  switch (c)
  {
    case 'A':
      return ATCG[0][0]+ATCG[1][0]+ATCG[2][0];
    case 'T':
      return ATCG[0][1]+ATCG[1][1]+ATCG[2][1];
    case 'C':
      return ATCG[0][2]+ATCG[1][2]+ATCG[2][2];
    case 'G':
      return ATCG[0][3]+ATCG[1][3]+ATCG[2][3];
  }
  throw GenException("In function getNucleotideSums char is not a nucleotide.");
}

long long GenCodons::getCodonCount(const Codon& codon) const
{
  return codonsTable[codon.getNucleotideInt(0)][codon.getNucleotideInt(1)][codon.getNucleotideInt(2)];
}
long long GenCodons::getCodonCount(const int n1,const int n2,const int n3) const
{
  return codonsTable[n1][n2][n3];
}

long long GenCodons::getCodonCount(const char c1,const char c2,const char c3) const
{
  char tab[3]={c1,c2,c3};
  int numberTab[3];
  for(int i=0;i<3;i++)
  {
    switch (tab[i])
    {
      case 'A':
        numberTab[i]=0;
        break;
      case 'T':
        numberTab[i]=1;
        break;
      case 'C':
        numberTab[i]=2;
        break;
      case 'G':
        numberTab[i]=3;
        break;
      default:
        throw GenException ("In function getCodonCount char is not correct nucleotide.");
    }
  }
  return getCodonCount(numberTab[0], numberTab[1], numberTab[2]);
}

GenInfo::GenInfo()
{
  areInCodonFrequencyTableCorrectValues=0;
  areInNucleotideFrequencyTableCorrectValues=0;
  areInCodonPredictedFrequencyTableCorrectVales=0;
}

void GenInfo::createNucleotideFreqencyTable(const GenSumarize& genS)
{
  areInNucleotideFrequencyTableCorrectValues=1;
  for(int p=0;p<3;p++)
  {
    double nucleotidesOnP=genS.getNucleotideSum(p);
    for(int i=0;i<4;i++)
    {
      nucleotideFrequencyOnPositionsTable[p][i]=genS.getNucleotides(i,p)/nucleotidesOnP;
    }
  }
}

void GenInfo::createCodonFrequencyTable(const GenSumarize& genS)
{
  areInCodonFrequencyTableCorrectValues=1;
  double codonSum=genS.getNonCorrectCodons()+genS.getSumOfCorrectCodons();
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        codonFrequencyTable[i][j][k]=genS.getCodonCount(i,j,k)/codonSum;
      }
    }
  }
}

double GenInfo::getFrequecyOfNucleotideOnPosition(const int n, const int p) const
{
  if(!areInNucleotideFrequencyTableCorrectValues)
  {
    throw GenException("In function getFrequecyOfNucleotideOnPosition there is no correct values in nucleotideFrequencyOnPositionsTable.");
  }
  return nucleotideFrequencyOnPositionsTable[p][n];
}

void GenInfo::createCodonPredictedFrequencyTable()
{
  areInCodonPredictedFrequencyTableCorrectVales=1;
  if(areInNucleotideFrequencyTableCorrectValues==0)
  {
    throw GenException ("Attempt to createCodonPredictedFrequencyTable without correct nucleotideFrequencyOnPositionsTable.");
  }
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        codonPredictedFrequencyTable[i][j][k]=getFrequecyOfNucleotideOnPosition(i,0)*
          getFrequecyOfNucleotideOnPosition(j,1)*
          getFrequecyOfNucleotideOnPosition(k,2);
      }
    }
  }
}

double GenInfo::getPredictedCodonFrequency(const Codon& codon) const
{
  if(!areInCodonPredictedFrequencyTableCorrectVales)
  {
    throw GenException("Attempt to getPredictedCodonFrequency without correct codonPredictedFrequencyTabl.");
  }
  return codonPredictedFrequencyTable[codon.getNucleotideInt(0)][codon.getNucleotideInt(1)][codon.getNucleotideInt(2)];
}

double GenInfo::getPredictedCodonFrequency(const int nuc1, const int nuc2, const int nuc3) const
{
  if(!areInCodonPredictedFrequencyTableCorrectVales)
  {
    throw GenException("Attempt to getPredictedCodonFrequency without correct codonPredictedFrequencyTabl.");
  }
  return codonPredictedFrequencyTable[nuc1][nuc2][nuc3];
}

double GenInfo::getCodonFreqeuncy(const int nuc1, const int nuc2, const int nuc3) const
{
  if(!areInCodonFrequencyTableCorrectValues)
  {
    throw GenException("Attempt to getCodonFreqeuncy without correct codonFrequencyTable.");
  }
  return codonFrequencyTable[nuc1][nuc2][nuc3];
}

double GenInfo::getPredictedCodonFrequency(const char c1, const char c2, const char c3) const
{
  char helpTab[]={c1,c2,c3};
  int tab[3];
  for(int i=0;i<3;i++)
  {
    switch (helpTab[i])
    {
      case 'A':
        tab[i]=0; 
        break;
      case 'T':
        tab[i]=1;
        break;
      case 'C':
        tab[i]=2;
        break;
      case 'G':
        tab[i]=3;
        break;
      default:
        throw GenException((std::string)&helpTab[i] + " is not correct nucleotide.");
    }
  }
  return getPredictedCodonFrequency(tab[0],tab[1],tab[2]);
}
