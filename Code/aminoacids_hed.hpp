#ifndef AMINOACIDS_HED
#define AMINOACIDS_HED

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include "excpetions_hed.hpp"
#include "tables_hed.hpp"

extern std::vector<char> nucleotideCharList;

class Nucleotide
///class which represent a nucleotide
{
  private:
    char c;
    int n;

    ///convert char to integer representation
    int charToInt() const;

  public:
    ///Create nucleotide with symbolic letter 
    Nucleotide(char);

    ///get integer representation of nucleotide
    int getNucleotideInt() const noexcept;
    ///get symbolic representation of nucleotide 
    char getNucleotideChar() const noexcept;
    
};

class Codon
{
  private:
    Nucleotide n[3];
  public:
    Codon(const std::string& seq);
    Codon(const char c1, const char c2, const char c3);
    Codon(const Nucleotide& n1, const Nucleotide& n2, const Nucleotide& n3);

    ///get integer representation of nucleotide on position p (p in {0,1,2})
    int getNucleotideInt(int p) const;
    ///get char representation of nucleotide on position p (p in {0,1,2})
    char getNucleotideChar(int p) const;
    ///get representation of codon as std::string
    std::string getString() const;
};


class Aminoacid
{
  private:
    Codon seq;
    char symbol;
    
    ///List of all one leter aminoacids symbols
    static std::vector<char> aminoacidsList;

    ///map in which key are aminoacids one letter symbols and values
    ///are vector of codons which code this aminoacid
    static std::unordered_map<char,std::vector<Codon>> codonsCodingAminoacid;
  public:
    Aminoacid(const Codon& s);
    Aminoacid(const char c1, const char c2, const char c3);

    ///get one letter aminoacid representation
    char getSymbol() const noexcept;

    ///get count of diffrent codons which code this aminoacid
    int codonsCount() const noexcept;

    ///get vector of all codons which code this aminoacid
    std::vector<Codon> codingCodons() const noexcept;

    ///Get list of all aminoacids symbols.
    static const std::vector<char>& getListOfAminoacids();

    ///Get vector with all codons coding given aminoacid
    static const std::vector<Codon>& codingCodons(char aa); 
};

int countCodonsCodingAminoacid(const Codon& codon);
#endif //AMINOACIDS_HED

