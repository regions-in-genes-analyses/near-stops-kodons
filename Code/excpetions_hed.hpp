#ifndef EXCEPTIONS_HED
#define EXCEPTIONS_HED
#include <stdexcept>

class ProgramException:public std::logic_error
{
  public:
    ProgramException(const std::string& s):logic_error(s)
  {}
};
class FnaFileException:public std::logic_error
{
  public:
    FnaFileException(const std::string& s):logic_error(s)
  {}
};
class GenException:public std::logic_error
{ 
  public:
    GenException(const std::string& s):logic_error(s)
  {}
};
class NucleotideException:public std::logic_error
{ 
  public:
    NucleotideException(const std::string& s):logic_error(s)
  {}
};
class EmptyRegionException:public GenException
{
  public:
    EmptyRegionException(const std::string& s) :GenException(s)
  {}
};
class NonTreeDividedGenException:public GenException
{
  public:
    NonTreeDividedGenException(const std::string& s):GenException(s)
  {}
};
class TooShortGenException:public GenException
{
  public:
    TooShortGenException(const std::string& s):GenException(s)
  {}
};
class GenToOmit:public GenException
{
  public:
    GenToOmit(const std::string& s):GenException(s)
    {}
};

#endif //EXCEPTIONS_HED
