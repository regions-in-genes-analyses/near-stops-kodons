#ifndef TABLES_HED
#define TABLES_HED
#include <string>
#include <vector>

extern char DNACodonTable[4][4][4];
extern double DNACodonWeight[4][4][4];

std::vector<std::string>& DangerousCodonsTable();
std::vector<std::string>& DangerousCodonsWithAlternative();
std::vector<std::string>& DangerousCodonsWithoutAlternative();
std::vector<std::string>& DoubleDangerousCodonsTable();



#endif //TABLES_HED
