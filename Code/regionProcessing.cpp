#include "regionProcessing_hed.hpp"

RiskScoreOmega::RiskScoreOmega([[maybe_unused]] const bool B)
{
  ifCorrectState=false;
  return;
}

RiskScoreOmega::RiskScoreOmega(const GenRegion& genRegion, const GenInfo& genI)
{
  sequence=0;
  random=0;
  if (ProgramSettings::normalizationType!=ProgramSettings::omega)
  {
    ifCorrectState=false;
    return;
  }
  ifCorrectState=true;
//  std::cerr<<"Kostruktor Omegi.\n";
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        switch(ProgramSettings::omegaRandomType)
        {
          case 0:
            AddToRandomType0Score(genRegion,
                Codon(nucleotideCharList[i],nucleotideCharList[j],nucleotideCharList[k]));
            break;
          case 1:
            AddToRandomType1Score(genRegion,
                Codon(nucleotideCharList[i],nucleotideCharList[j],nucleotideCharList[k]),genI);
            break;
          case 2:
            AddToRandomType2Score(genRegion,
                Codon(nucleotideCharList[i],nucleotideCharList[j],nucleotideCharList[k]),genI);
            break;
          default:
            throw ProgramException("Illegal omega random type. Given omega random type:"+std::to_string(ProgramSettings::omegaRandomType));
        }
        AddToSequenceScore(genRegion,Codon(nucleotideCharList[i],nucleotideCharList[j],nucleotideCharList[k]));
      }
    }
  }
}

RiskScoreOmega::RiskScoreOmega(const RiskScoreOmega& O)
{
  ifCorrectState=O.ifCorrectState;
  sequence=O.sequence;
  random=O.random;
}

void RiskScoreOmega::AddToRandomType0Score(const GenRegion& genRegion,const Codon& codon)
{
  double aw=genRegion.getAminoacidCount(Aminoacid(codon))*
    DNACodonWeight[codon.getNucleotideInt(0)][codon.getNucleotideInt(1)][codon.getNucleotideInt(2)];

  random+=(aw/(double)Aminoacid(codon).codonsCount());
}

void RiskScoreOmega::AddToRandomType1Score(const GenRegion& genRegion, const Codon& codon, const GenInfo& genI)
{
  random+=(genI.getPredictedCodonFrequency(codon)*genRegion.getLengthInCodons()*
        DNACodonWeight[codon.getNucleotideInt(0)][codon.getNucleotideInt(1)][codon.getNucleotideInt(2)]);
}

void RiskScoreOmega::AddToRandomType2Score(const GenRegion& genRegion, const Codon& codon, const GenInfo& genI)
{
  double fractionDenominator=0;
  std::vector<Codon> allCodonsCodingTheSameAminoacid=Aminoacid(codon).codingCodons();
  for(unsigned int i=0;i<allCodonsCodingTheSameAminoacid.size();i++)
  {
    fractionDenominator+=genI.getPredictedCodonFrequency(allCodonsCodingTheSameAminoacid[i]);
  }
  if (fractionDenominator==0)
  {
      return;
  }
  double fraction = genI.getPredictedCodonFrequency(codon)/fractionDenominator;
  random+=genRegion.getAminoacidCount(Aminoacid(codon))*
        DNACodonWeight[codon.getNucleotideInt(0)][codon.getNucleotideInt(1)][codon.getNucleotideInt(2)]*fraction;
}
void RiskScoreOmega::AddToSequenceScore(const GenRegion& genRegion, const Codon& codon)
{
  sequence+=(double)genRegion.getCodonCount(codon)*
    (double)DNACodonWeight[codon.getNucleotideInt(0)][codon.getNucleotideInt(1)][codon.getNucleotideInt(2)];
}

double RiskScoreOmega::getSequenceScore() const
{
  if(!ifCorrectState)
    throw ProgramException("Using uncorrect initialized RiskScoreOmega.");
  return sequence;
}
double RiskScoreOmega::getRandomScore() const
{
  if(!ifCorrectState)
    throw ProgramException("Using uncorrect initialized RiskScoreOmega.");
  return random;
}
double RiskScoreOmega::getF() const
{
  if(!ifCorrectState)
    throw ProgramException("Using uncorrect initialized RiskScoreOmega.");
  return (double)sequence/random;
}

RegionResult::RegionResult(int n, long long c, long long l, std::string s, const RiskScoreOmega& O):
  ifFrequenciesAreCorrect(false),
  normalizedFrequency(0),predictedFrequency(0),ifErrorOccur(false), 
  regionNumber(n), regionLabel(s),countOfLookedCodons(c),
  regionLengthInCodons(l),
  omega(O)
{}

RegionResult::RegionResult():omega(false)
{
  ifFrequenciesAreCorrect=false;
  ifErrorOccur=false;
  regionNumber=-1;
  countOfLookedCodons=0;
  regionLengthInCodons=0;
  normalizedFrequency=0;
  predictedFrequency=0;
}
RegionResult::RegionResult(int n, bool e):omega(false)
{
  ifFrequenciesAreCorrect=false;
  ifErrorOccur=e;
  regionNumber=n;
  countOfLookedCodons=0;
  regionLengthInCodons=0;
  normalizedFrequency=0;
  predictedFrequency=0;
}

RegionResult& RegionResult::operator+(const RegionResult& reg)
{
  countOfLookedCodons+=reg.countOfLookedCodons;
  regionLengthInCodons+=reg.regionLengthInCodons;
  return *this;
}
double RegionResult::getNormalizedFrequency() const
{
  if(ifFrequenciesAreCorrect==false)
  {
    throw GenException("Getting normalized frequency without previus setting.");
  }
  return normalizedFrequency;
}

double RegionResult::getRealFrequency() const
{
  if(ifFrequenciesAreCorrect==false)
  {
    throw GenException("Getting normalized frequency without previus setting.");
  }
  return realFrequency;
}

double RegionResult::getPredictedFrequency() const
{
  if(ifFrequenciesAreCorrect==false)
  {
    throw GenException("Getting predicted frequency without previous setting by running normalization function.");
  }
  return predictedFrequency;
}

RegionResult lookForCodonsInRegion(const GenRegion& reg, const std::vector<std::string>& lookedCodons, const GenInfo& genI)
{
  int countOfLookedCodons=0;
  for(unsigned int it=0;it<lookedCodons.size();it++)
  {
    countOfLookedCodons+=reg.getCodonCount(lookedCodons[it][0],lookedCodons[it][1],lookedCodons[it][2]);
  }
//  std::cerr<<"==="<<countOfLookedCodons<<"\n";
  RiskScoreOmega omega=RiskScoreOmega(reg,genI);
  return RegionResult(reg.number, countOfLookedCodons, reg.sequence.size()/3, reg.label,omega);
}

void RegionResult::printDebugInfo()
{
  std::cerr<<"ifErrorOccur: "<<ifErrorOccur<<"\n";
  std::cerr<<"regionNumber: "<<regionNumber<<"\n";
  std::cerr<<"countOfLookedCodons: "<<countOfLookedCodons<<"\n";
  std::cerr<<"regionLengthInCodons: "<<regionLengthInCodons<<"\n";
}

void RegionResult::setFrequencies(const GenInfo& genI, const std::vector<std::string>& pattern)
{
  ifFrequenciesAreCorrect=true;

  if(ProgramSettings::normalizationType<2)
  {
    double value=0;

    for(unsigned int i=0;i<pattern.size();i++)
    {
      value+=genI.getPredictedCodonFrequency(pattern[i][0],pattern[i][1],pattern[i][2]);
    }

    if(ProgramSettings::normalizationType==0)
    {
      realFrequency=countOfLookedCodons/(double)regionLengthInCodons;
      if(value==0 and realFrequency!=0)
      {
        throw GenException ("Sum of predicted codons frequecy is 0 and Sum of real codon frequency is non-zero.");
      }
      predictedFrequency=value;
      normalizedFrequency=realFrequency/value;
      return ;
    }
    else //(ProgramSettings::normalizationType==1) ///<remainder
    {
      realFrequency=countOfLookedCodons/(double)regionLengthInCodons;
      predictedFrequency=value;
      normalizedFrequency=realFrequency-predictedFrequency;
      return ;
    }
  }
  if(ProgramSettings::normalizationType==2) ///<omega
  {
    realFrequency=omega.getSequenceScore();
    predictedFrequency=omega.getRandomScore();
    normalizedFrequency=omega.getF();
    return;
  }
  else
  {
    throw ProgramException("Normalization type is not recognized.");
  }
}
