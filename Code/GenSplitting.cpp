#include "GenSplitting_hed.hpp"

GenRegion::GenRegion():aminoacids({
    {'A',0}, {'C',0}, {'D',0}, {'E',0},
    {'F',0}, {'G',0}, {'H',0}, {'I',0},
    {'K',0}, {'L',0}, {'M',0}, {'N',0},
    {'P',0}, {'R',0}, {'S',0}, {'T',0},
    {'W',0}, {'Q',0}, {'V',0}, {'X',0},
    {'Y',0} })
{
  sequence="";
  number=-1;
  length=0;
}

GenRegion::GenRegion(int nr, std::string&& sequ) noexcept:
  aminoacids({
    {'A',0}, {'C',0}, {'D',0}, {'E',0},
    {'F',0}, {'G',0}, {'H',0}, {'I',0},
    {'K',0}, {'L',0}, {'M',0}, {'N',0},
    {'P',0}, {'R',0}, {'S',0}, {'T',0},
    {'W',0}, {'Q',0}, {'V',0}, {'X',0},
    {'Y',0} }),sequence(sequ),number(nr)
{
  int codonCount=sequence.size()/3;
  for(int i=0;i<codonCount;i++)
  {
    addCodon(sequence[i*3], sequence[i*3+1], sequence[i*3+2]);
    try
    {
      char s=Aminoacid(sequence[i*3], sequence[i*3+1], sequence[i*3+2]).getSymbol();
      aminoacids[s]+=1;
    }
    catch (NucleotideException& e)
    {}
  }
  length=getSumOfCorrectCodons();
}

double GenRegion::codonFrequency(const Codon& codon) const
{
  double freq = this->getCodonCount(codon);
  return freq/getLengthInCodons();
}

std::vector<double> GenRegion::vectorOfCodonsFrequencies() const
{
  std::vector<double> freqs;
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        Codon codon=Codon(nucleotideCharList[i], nucleotideCharList[j], nucleotideCharList[k]);
        freqs.push_back(this->codonFrequency(codon));
      }
    }
  }
  return freqs;
}

std::vector<std::string> GenRegion::labelsOfvectorOfCodonsFrequencies()
{
  std::vector<std::string> labels;
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        std::string lab="";
        lab+=nucleotideCharList[i];
        lab+=nucleotideCharList[j];
        lab+=nucleotideCharList[k];
        lab+="_cod_freq_reg";
        labels.push_back(lab);
      }
    }
  }
  return labels;
}

int GenRegion::getAminoacidCount(const Aminoacid& a) const
{
  return aminoacids.at(a.getSymbol());
}

long long GenRegion::getLengthInCodons() const
{
  return length;
}

void GenRegion::combine(GenRegion& reg)
{
  sequence+=reg.sequence;
  nonCorrectCodon+=reg.nonCorrectCodon;
  length+=reg.length;
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        codonsTable[i][j][k]+=reg.codonsTable[i][j][k];
      }
    }
  }
  const std::vector<char>& aminoacidsList=Aminoacid::getListOfAminoacids();
  for (unsigned int i=0;i<aminoacidsList.size();i++)
  {
    aminoacids[aminoacidsList[i]]+=reg.aminoacids[aminoacidsList[i]];
  }
}

std::ostream& operator<<(std::ostream& os,const GenRegion& reg)
{
  os<<"Region nr: "<<reg.number<<", Region label: "<<reg.label<<", sequence: "<<reg.sequence;
  return os;
}

void splitGenAccordingToResolution(const Gen& gen,
    const GenSplittingTemplate& genTemplate, std::vector<GenRegion>& reg) noexcept
{
  int genLength=gen.sequence.size();
  double avaRegionLengthInNuc=genLength/(double)genTemplate.resolution;
  int nr=0;
  double teoreticRegionEnd=avaRegionLengthInNuc;
  for(int firstNuc=0;firstNuc<genLength;nr++, teoreticRegionEnd+=avaRegionLengthInNuc)
    ///firstNuc - first Nucleotide which shoud be add to the next region 
  {
    int length=std::round(teoreticRegionEnd)-firstNuc;
    if(length==-1)
    {
      length=0;
    }
    else if(length%3==1)
    {
      length--;
    }
    else if (length%3==2)
    {
      length++;
    }
    reg.emplace_back(nr, gen.sequence.substr(firstNuc, length));
    firstNuc+=length;
  }
}

void combineSplitedGenAccordingToTemplate(const GenSplittingTemplate& genTemplate,
    std::vector<GenRegion>& reg) 
{
  int numberOfRegions=genTemplate.regionLength.size();
  int k=0;
  double beginingInDecimal=0, endInDecimal=0;
  double resolutionInDecimal=1.0/genTemplate.resolution;
  for(int i=0;i<numberOfRegions;i++)
  {
    //TODO:using move constructor
    beginingInDecimal=endInDecimal;

    reg[i]=reg[k];
    k++;
    endInDecimal+=resolutionInDecimal;
    for(int j=1;j<genTemplate.regionLength[i];j++)
    {
      reg[i].combine(reg[k]);
      k++;
      endInDecimal+=resolutionInDecimal;
    }
    if(reg[i].sequence.size()==0)
    {
      throw EmptyRegionException("There is empty region "+std::to_string(i));
    }
    reg[i].label=std::to_string(std::round(beginingInDecimal*100)/100.0).substr(0,4)
      +"-"+std::to_string(std::round(endInDecimal*100)/100.0).substr(0,4);
  }
  reg.resize(numberOfRegions);
}

GenSplittingTemplate::GenSplittingTemplate(int res, std::vector<int>&& rL) noexcept:
resolution(res), regionLength(rL)
{ }

void GenSplittingTemplate::recreate(int res, std::vector<int>&& rL) noexcept
{
  resolution=res;
  regionLength=rL;
}
