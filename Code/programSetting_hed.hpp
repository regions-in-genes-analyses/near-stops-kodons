#ifndef PROGRAMSETTING_HED
#define PROGRAMSETTING_HED
#include "GenSplitting_hed.hpp"
#include "tables_hed.hpp"
#include <fstream>
#include <boost/filesystem.hpp>

class ProgramSettings
{
  public:
    static bool ifCommaInOutput;
    static bool ifNewPatternForLokingCodons;
    static bool ifGnuplotFile;

    static unsigned int omegaRandomType;

    static unsigned int minSequenceLengthInCodons;
    static unsigned int maxSequenceLengthInCodons;
    static GenSplittingTemplate splittingTemplate;
    static const std::vector<std::pair<std::vector<std::string>,std::string>>& getPatternTabForLookingCodons()
        {return patternTablesForLookingCodons;}
    static void addNewCodonGroupToPatternLookingTab(std::vector<std::string>&&, std::string&&);
    static enum normalizationTypes
    {
      divide=0,
      remainder, 
      omega
    } normalizationType;
    static void createGnuplotFile(std::string &);
    static std::ofstream* getGnuplotFile();
    static void createSharedGlobal_dcFile(std::string&);
    static std::ofstream* getSharedGlobal_dcFile();
    static bool ifSharedGlobal_dcFile;
    static bool regionDebug;
  private:
    static std::ofstream* sharedGlobal_dcFile;
    static std::ofstream* gnuplotFile;
    static std::vector<std::pair<std::vector<std::string>, std::string>> patternTablesForLookingCodons;
};

#endif //PROGRAMSETTING_HED
