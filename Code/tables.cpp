#include "tables_hed.hpp"


char DNACodonTable[4][4][4]={
  //AXX
  {
    //AAX
    {'K','N','N','K'},
    //ATX
    {'I','I','I','M'},
    //ACX
    {'T','T','T','T'},
    //AGX
    {'R','S','S','R'}},

  //TXX
  {
    //TAX
    {'X','Y','Y','X'},
    //TTX
    {'L','F','F','L'},
    //TCX
    {'S','S','S','S'},
    //TGX
    {'X','C','C','W'}
  },
  //CXX
  {
    //CAX
    {'Q','H','H','Q'},
    //CTX
    {'L','L','L','L'},
    //CCX
    {'P','P','P','P'},
    //CGX
    {'R','R','R','R'}
  },
  //GXX
  {
    //GAX
    {'E','D','D','E'},
    //GTX
    {'V','V','V','V'},
    //GCX
    {'A','A','A','A'},
    //GGX
    {'G','G','G','G'}
  }
};

std::vector<std::string>& DangerousCodonsTable()
{
  static std::vector<std::string> *x=new std::vector<std::string>
  {"AAA","AAG","AGA","CAA","CAG","CGA","GAA","GAG","GGA","TAC","TAT","TCA","TCG","TGC","TGG","TGT","TTA","TTG"};
  return *x;
}

std::vector<std::string>& DangerousCodonsWithAlternative()
{
  static std::vector<std::string> *x=new std::vector<std::string>
  {"AGA","CGA","GGA","TCA","TCG","TTA","TTG"};
  return *x;
}

std::vector<std::string>& DangerousCodonsWithoutAlternative()
{
  static std::vector<std::string> *x=new std::vector<std::string>
  {"AAA","AAG","CAA","CAG","GAA","GAG","TAC","TAT","TGC","TGG","TGT"};
  return *x;
}
std::vector<std::string>& DoubleDangerousCodonsTable()
{
  static std::vector<std::string> *x=new std::vector<std::string>
  {"TAC","TAT","TCA","TGG","TTA"};
  return *x;
}

double DNACodonWeight[4][4][4]={
  //AXX
  {
    //AAX
    {1,0,0,1},
    //ATX
    {0,0,0,0},
    //ACX
    {0,0,0,0},
    //AGX
    {1,0,0,0}},

  //TXX
  {
    //TAX
    {10000000,2,2,10000000},
    //TTX
    {2,0,0,1},
    //TCX
    {2,0,0,1},
    //TGX
    {10000000,1,1,2}
  },
  //CXX
  {
    //CAX
    {1,0,0,1},
    //CTX
    {0,0,0,0},
    //CCX
    {0,0,0,0},
    //CGX
    {1,0,0,0}
  },
  //GXX
  {
    //GAX
    {1,0,0,1},
    //GTX
    {0,0,0,0},
    //GCX
    {0,0,0,0},
    //GGX
    {1,0,0,0}
  }
};


