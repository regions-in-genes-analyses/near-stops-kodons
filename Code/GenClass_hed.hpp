#ifndef GENCLASS_HED
#define GENCLASS_HED
#include<string>

class Gen
{
  public:
    std::string header;
    std::string sequence;
    std::string lastTreeNucleotides;
	std::string errorMessage;

    Gen(std::string hed, std::string sequ, std::string mess):header(hed),sequence(sequ), errorMessage(mess){}
    Gen(){errorMessage="-";}

};
#endif 
