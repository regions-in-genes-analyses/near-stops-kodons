#ifndef GENSPLITTING_HED
#define GENSPLITTING_HED

#include "GenSumarizeClass_hed.hpp"
#include "GenClass_hed.hpp"
#include <vector>
#include <cmath>
#include <iostream>

class GenRegion:public GenCodons
{
  private:
    ///Map which map to each aminoacid (in form of one char) 
    ///count of his occurence in region.
    std::unordered_map<char, int> aminoacids;
    
    ///Length in codons, only correct codons are counting.
    ///It is initialized by GenRegion construction
    long long length;
  public:
    std::string sequence;

    ///Order region number, counting from the beginning.
    int number;
    std::string label;

    ///Deflaut constructor, contains no codons, length is 0,
    ///have empty sequence and order number is -1
    GenRegion();
    ///Create GenRegion with order number nr, and sequence seq.
    ///Length and count of aminoacid are initialized base on sequence.
    GenRegion(int nr, std::string&& sequ) noexcept;

    ///Return length in correct codons (count of correct codons in region).
    long long getLengthInCodons() const;

    ///Merge two regions, lengths are added, aminoacids are added, 
    //.sequences and non correct nucleotides count are added.
    void combine (GenRegion&); 

    ///Return count of aminoacids in region.
    int getAminoacidCount(const Aminoacid&) const;

    ///Return frequency of one codon in region
    double codonFrequency(const Codon&) const;
    
    ///Return vector of codons frequencies in region ordered by nucleotideCharList
    std::vector<double> vectorOfCodonsFrequencies() const;
    
    ///Return vector of labels for codon frequencies 
    ///(the same order as returned by vectorOfCodonsFrequencies() )
    static std::vector<std::string> labelsOfvectorOfCodonsFrequencies();

    friend std::ostream& operator<< (std::ostream&, const GenRegion&);
};

class GenSplittingTemplate
{
  public:
    GenSplittingTemplate(int res, std::vector<int>&& rL) noexcept;
    void recreate(int res, std::vector<int>&& rL) noexcept;
    int resolution;
    std::vector<int> regionLength;
};
void splitGenAccordingToResolution(const Gen&, const GenSplittingTemplate&, std::vector<GenRegion>&) noexcept;
void combineSplitedGenAccordingToTemplate(const GenSplittingTemplate&, std::vector<GenRegion>&);

#endif //GENSPLITTING_HED
