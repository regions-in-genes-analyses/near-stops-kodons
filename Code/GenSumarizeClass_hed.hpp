#ifndef GENSUMARIZE_HED
#define GENSUMARIZE_HED

#include"excpetions_hed.hpp"
#include "aminoacids_hed.hpp"

class GenCodons
{
  protected:
    long long codonsTable[4][4][4];
    long long nonCorrectCodon;
  public:
    GenCodons();

    long long getSumOfCorrectCodons() const;
    long long getNonCorrectCodons() const;

    long long getCodonCount(const char c1,const char c2,const char c3) const;
    long long getCodonCount(const int n1,const  int n2, const int n3) const;
    long long getCodonCount(const Codon& codon) const;

    void addCodon (const char c1,const char c2,const char c3);
};

class GenNucleotides
{
  protected:
    long long ATCG[3][4];  ///< first position in codon, second places the same like in name
    long long nonCorrectNucleotide;
  public:
    GenNucleotides();

    void addNucleotide (const char c, const unsigned short k);///< k position mod 3

    long long getNucleotides(const char c, const int k) const;
    long long getNucleotides(const int n, const int p) const;
    long long getNucleotideSum(const char c) const;
    long long getNucleotideSum(const int k) const;

    long long getSumOfAllCorrectNucleotide() const;
    long long getNonCorrectNucleotide() const;
};

class GenSumarize:public GenCodons, public GenNucleotides
{
  public:
    GenSumarize();
    GenSumarize& combineGens(const GenSumarize&);
};

class GenInfo
{
  private:
    bool areInNucleotideFrequencyTableCorrectValues;
    bool areInCodonFrequencyTableCorrectValues;
    bool areInCodonPredictedFrequencyTableCorrectVales;

    double nucleotideFrequencyOnPositionsTable[3][4];
    double codonFrequencyTable[4][4][4];
    double codonPredictedFrequencyTable[4][4][4];
  public:
    GenInfo();

    void createNucleotideFreqencyTable(const GenSumarize& genS);
    void createCodonFrequencyTable(const GenSumarize& genS);
    void createCodonPredictedFrequencyTable();

    double getFrequecyOfNucleotideOnPosition(const int nuc, const int pos) const;
    double getCodonFreqeuncy(const int nuc1, const int nuc2, const int nuc3) const;
    double getPredictedCodonFrequency(const Codon& codon) const;
    double getPredictedCodonFrequency(const int nuc1, const int nuc2, const int nuc3) const;
    double getPredictedCodonFrequency(const char nuc1, const char nuc2, const char nuc3) const;
};

inline int nucleotideToInt(const char c)
{
  switch (c)
  {
    case 'A':
      return 0;
    case 'T':
      return 1;
    case 'C':
      return 2;
    case 'G':
      return 3;
    default:
      throw GenException ("In function getCodonCount char is not correct nucleotide.");
  }
}
#endif //GENSUMARIZE_HED
