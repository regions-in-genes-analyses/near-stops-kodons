#include "main_hed.hpp"


void printUsage()
{
  std::cout<<"Options: \n";
  std::cout<<"-i FILE   - FILE in .fna format with genome sequences, which should be analyzed\n";
  std::cout<<"-id DIRECTORY   - DIRECTORY with .fna filles (now version 0.3.4, shouldn't have any other files)\n";
  std::cout<<"-o PATH   - output files will be generate by adding file extension (see 3.) to PATH \n\
      when there is no -o option output files will be generate base on input filename,\n\
      .fna extension will be removed and propery output file extension will be added\n";
  std::cout<<"-od DIRECTORY   - DIRECTORY in which will be generated output files for all files from -id\n\
      option, filename will be generated base on input file name (see -o)\n";
  std::cout<<"--comma   - use in output files comma as decimal separator (deflaut: use punct)\n";
  std::cout<<"--region-template n T   - description of template which should be used to split gens \n\
      into regions, n is integer number >0 which is split resolution and T is parts \n\
      combining template in form : a1,a2,a3,...,ak where for each ai, ai is integer >0 \n\
      and sum of all ai is n. Firstly gen will be split in n almost equal parts \n\
      (diffrent parts length can be result of gen length which is non divided by n) \n\
      and secondly this parts will be combine according to T. In i-th region will \n\
      be ai resolution parts.\n"; 
  std::cout<<"--region-template-f FILE  - FILE with region splitting template where in first line\n\
      is n and in second line is T (see --region-template)\n";
  std::cout<<"--codon-group FILE  - FILE which contain in first line name of the group and in the second\n\
      line codon writen by capital letters and separate by commas. For example:\n\
          name_group\n\
          AAA,ATG,TGA,CGA\n";
  std::cout<<"--max n   - process only gens which has length in codons not greater than n\n";
  std::cout<<"--min n   - process only gens which has length in codons not less than n\n";
  std::cout<<"--divides  - normalize by divide.\n";
  std::cout<<"--omega MATRIX_FILE  RANDOM_NUMBER -normalize by \u03a9.\n\
According to: Schmid and Flegel Journal of Translational Medicine 2011, 9:87\n\
http://www.translational-medicine.com/content/9/1/87\n\
RANDOM_NUMBER can be:\n\
\t 0 - normalize by aminoacids counts\n\
\t 1 - normalize by predicted codons count\n\
\t 2 - normalize by predicted part in aminoacid count\n";
  std::cout<<"--shared-global_dc DIRECTORY - create one shared file of type global_dc for all genoms in DIRECTORY \n";

  std::cout<<"-h --help   - print options descriptions\n";
  std::cout<<"--region-debug\n";
}

void putFilesFromInputDictonaryIntoSpecies(std::string inDicPath, std::vector<SpeciesFiles>&speciesFiles)
{
  boost::filesystem::recursive_directory_iterator end;
  for(boost::filesystem::recursive_directory_iterator dI (inDicPath);dI!=end;dI++)
  {
    speciesFiles.emplace_back(dI->path().generic_string());
  }
}

void createRegionsTemplate(int regionsNumber,const std::string& splittingTemplate)
{
  if(splittingTemplate[0]=='-')
  {
    std::cerr<<"Second argument after --region-template should be template of combining resolutions parts.\n";
    exit(1);
  }
  std::stringstream ss(splittingTemplate);
  std::vector<int> help;
  int a, k=0;
  char c;
  while(!ss.eof())
  {
    ss.get(c);
    if(std::isdigit(c))
    {
      ss.unget();
      ss>>a;
      k+=a;
      help.push_back(a);
    }
  }
  if(regionsNumber!=k)
  {
    throw ProgramException("In input there is bad region splitting template.");
  }
  ProgramSettings::splittingTemplate.recreate(regionsNumber, std::move(help));
}

void prepareCodonGroup(std::istream& codonGroupFile)
{
  std::string name;
  std::string line;
  std::string additionalString;

  while(!codonGroupFile.eof())
  {
    codonGroupFile>>name;
    std::getline(codonGroupFile >>std::ws,line);
    std::vector<std::string> help;
    for(unsigned int i=0;i<line.size();i+=3)
    {
      additionalString="";
      char c;
      for(int j=0;j<3 && i<line.size();j++)
      {
        c=line[i+j];
        if(std::isspace(c))
        {
          i++;
          continue;
        }
        if(c==',')
        {
          throw std::runtime_error("Error, parsing --codon-group file. Comma is to early.");
        }
        additionalString+=line[i+j];
      }
      c=line[i+3];
      while(c!=EOF && (c==',' || std::isspace(c)))
      {
        i++;
        c=line[i+3];
      }
      help.push_back(additionalString);
    }
    ProgramSettings::addNewCodonGroupToPatternLookingTab(std::move(help),std::move(name));
    codonGroupFile>>std::ws;
  }
}

void createCodonWeightTable(const std::string& matrixFileName)
{
  std::ifstream matrixFile;
  matrixFile.open(matrixFileName);
  if (matrixFile.fail())
  {
    throw std::runtime_error("Error while opening matrix file.");
  }
  //[i][j] i is turn into j
  double matrix[4][4];
  //atcg 
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      matrixFile>>matrix[i][j];
    }
  }
  for(int i=0;i<4;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<4;k++)
      {
        double sum=0;
        for (const Codon& stopCodon: Aminoacid::codingCodons('X'))
        {
          double weight=1;
          weight*=matrix[i][stopCodon.getNucleotideInt(0)];
          weight*=matrix[j][stopCodon.getNucleotideInt(1)];
          weight*=matrix[k][stopCodon.getNucleotideInt(2)];
          sum+=weight;
        }
        DNACodonWeight[i][j][k]=sum;
      }
    }
  }
  return;
}

void parseStartOptions(std::vector<std::string>& options, std::vector<SpeciesFiles>& speciesFiles)
{
  ///options are read from left to right when there are multiple
  ///files to process each -i is coresponding to the next -o if 
  ///between -i -o there is no another -i
  ///in this case first -i is one matching -o so the output filename
  ///is generating by program
  ///for example: -i A.in -i B.in -o C.out will cause that output from
  /// processing A.in will be in A.txt and output from B.in in C.out
  std::string inFilePath, outFilePath, inDicPath, outDicPath;
  bool controlMap[4]={0,0,0,0}; ///< controlMap[0] is for input, controlMap[1] for output
  ///< controlMap[2] is dicInput, controlMap[3] is dicOutput  
  for( std::vector<std::string>::iterator i=options.begin(); i!=options.end();i++)
  {
    try
    {
      if(*i == "--help" || *i == "-h")
      {
        printUsage();
        exit(0);
      }
      if(*i=="--comma")
      {
        ProgramSettings::ifCommaInOutput=1;
        continue;
      }
      if(*i=="-i")
      {
        if(controlMap[0]==1)
        {
          speciesFiles.emplace_back(inFilePath);
          controlMap[0]=0;
        }
        inFilePath=*(i+1);
        controlMap[0]=1;
        i++;///<skip file name
        continue;
      }
      if(*i=="-o")
      {
        outFilePath=*(i+1);
        controlMap[1]=1;
        if(controlMap[0] and controlMap[1])
        {
          speciesFiles.emplace_back(inFilePath,outFilePath);
          controlMap[0]=0;
          controlMap[1]=0;
        }
        i++;///<skip file name
        continue;
      }
      if(*i=="-id")
      {
        i++;
        if(controlMap[2]==1)
        {
          putFilesFromInputDictonaryIntoSpecies(inDicPath, speciesFiles);
        }
        inDicPath=*(i);
        controlMap[2]=1;
        continue;
      }
      if(*i=="-od")
      {
        i++;
        outDicPath=*(i);
        if(!boost::filesystem::exists(outDicPath))
        {
          boost::filesystem::create_directory(outDicPath);
        }
        if(!boost::filesystem::is_directory(outDicPath))
        {
          throw std::runtime_error("In -od : " +outDicPath + " exist and isn't directory.");
        }
        if(controlMap[2])
        {
          boost::filesystem::recursive_directory_iterator end;
          boost::filesystem::path inFilePath, outFilePath;
          for(boost::filesystem::recursive_directory_iterator dI (inDicPath);dI!=end;dI++)
          {
            inFilePath=dI->path();
            outFilePath=outDicPath;
            outFilePath/=inFilePath.filename();
            outFilePath.replace_extension();
            speciesFiles.emplace_back(inFilePath.generic_string(), outFilePath.generic_string());
          }
          controlMap[2]=0;
          continue;
        }
      }
      if(*i=="--region-template")
      {
        int regionsNumber=-1;
        std::string splittingTemplate;
        i++;
        regionsNumber=std::stoi(*i);
        i++;
        splittingTemplate=*i;
        createRegionsTemplate(regionsNumber,splittingTemplate);
        continue;
      }
      if(*i=="--region-template-f")
      {
        i++;
        std::ifstream regionTemplateFile;
        regionTemplateFile.open(*i);
        if(regionTemplateFile.fail())
        {
          throw std::runtime_error("Error while opening region template file.");
        }
        int regionsNumber;
        std::string splittingTemplate;
        regionTemplateFile>>regionsNumber>>std::ws;
        getline(regionTemplateFile,splittingTemplate);
        createRegionsTemplate(regionsNumber,splittingTemplate);
        continue;
      }
      if(*i=="--codon-group")
      {
        i++;
        std::ifstream codonGroupFile;
        codonGroupFile.open(*i);
        if(codonGroupFile.fail())
        {
          throw std::runtime_error("Error while opening codon group file: " + *i);
        }
        prepareCodonGroup(codonGroupFile);
        continue;
      }
      if(*i=="--remainder")
      {
        ProgramSettings::normalizationType=ProgramSettings::remainder;
        continue;
      }
      if(*i=="--divides")
      {
        ProgramSettings::normalizationType=ProgramSettings::divide;
        continue;
      }
      if(*i=="--omega")
      {
        ProgramSettings::normalizationType=ProgramSettings::omega;
        i++;
        std::string matrixFile=*i;
        createCodonWeightTable(matrixFile);
        ProgramSettings::addNewCodonGroupToPatternLookingTab(std::vector<std::string>(), "omega");
        i++;
        ProgramSettings::omegaRandomType=std::stoi(*i);
        continue;
      }
      if(*i=="--max")
      {
        i++;
        ProgramSettings::maxSequenceLengthInCodons=std::stoi(*i);
        continue;
      }
      if(*i=="--min")
      {
        i++;
        ProgramSettings::minSequenceLengthInCodons=std::stoi(*i);
        continue;
      }
      if(*i=="--shared-global_dc")
      {
        i++;
        ProgramSettings::ifSharedGlobal_dcFile=1;
        ProgramSettings::createSharedGlobal_dcFile(*i);
        continue;
      }
      if(*i=="--gnuplot-in-file")
      {
	      i++;
        ProgramSettings::ifGnuplotFile=1;
        ProgramSettings::createGnuplotFile(*i);
        continue;
      }
      if(*i=="--region-debug")
      {
        ProgramSettings::regionDebug=true;
        continue;
      }
      std::cerr<<"Option: "<<*i<<" isn't recognize.\n";
    }
    catch(std::ios_base::failure& error)
    {
      std::cerr<<"Error is appeared while opening file: \n"<<error.what()<<"\n";
      std::cerr<<"Program can be unstable.\n";
    }
  }
  if(controlMap[0]==1)
  {
    speciesFiles.emplace_back(inFilePath);
  }
  if(controlMap[2]==1)
  {
    putFilesFromInputDictonaryIntoSpecies(inDicPath, speciesFiles);
  }
  options.clear();
}

int main(int argc, char** argv)
{
  using namespace std;
  if(argc==1)
  {
    printUsage();
    exit(0);
  }  
  vector<string> options;
  for(int i=1;i<argc;i++)
  {
    options.push_back(argv[i]);
  }
  vector<SpeciesFiles> speciesFiles;
  parseStartOptions(options,speciesFiles);
  if(ProgramSettings::ifSharedGlobal_dcFile==1)
  {
    printHeaderOfADCFile(speciesFiles[0]);
  }
  for(std::vector<SpeciesFiles>::iterator i=speciesFiles.begin();i!=speciesFiles.end();i++)
  {
    (*i).open();
    parseOneFnaFile(*i);
    (*i).close();
  }

  if(ProgramSettings::getGnuplotFile()!=nullptr && ProgramSettings::getGnuplotFile()->is_open())
  {
	  ProgramSettings::getGnuplotFile()->close();
  }
  if(ProgramSettings::ifSharedGlobal_dcFile && ProgramSettings::getSharedGlobal_dcFile()->is_open())
  {
    ProgramSettings::getSharedGlobal_dcFile()->close();
    std::cerr<<"WARNING: Shared global_dc is open after processing all genomes. Closing shared global_dc. Please report this and check integrity of data.";
  }
}
