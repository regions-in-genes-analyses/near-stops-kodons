COM=g++
NAME=NSK
CFILES= $(wildcard Code/*.cpp)
OFILES= $(patsubst %.cpp, %.o, $(CFILES))
HFILES= $(wildcard Code/*.hpp)
CFLAGS= -Wextra -Wall -std=c++14 -g
LFLAGS= -lboost_system -lboost_filesystem
CLEANFILE= namesCleaner/main.cpp
MEANFILE= Avarage/main.cpp
STOPFINDERFILE= StopFinder/main.cpp


all: $(OFILES)
	$(COM) $(CFLAGS) $^ -o $(NAME) $(LFLAGS)

$(OFILES): %.o: %.cpp $(HFILES)
	$(COM) $(CFLAGS) -c $< -o $@ $(LFLAGS)

clear:
	rm -f $(OFILES) $(NAME) Code/*~ core
	rm -f ./*~
	rm -f ./namesCleaner/*~

cleanFile:
	$(COM) $(CFLAGS) $(CLEANFILE) -o namesCleaner/CleanNames

mean:
	$(COM) $(CFLAGS) $(MEANFILE) -o Avarage/calculateMean $(LFLAGS)

stopFinder:
	$(COM) $(CFLAGS) $(STOPFINDERFILE) -o StopFinder/findStops 
